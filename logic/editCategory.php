<?php
require("../logic/connection.php");
require("../class/category.php");
require("../views/indexCategory.php");
$connection = Connect();

if (isset($_POST['updateData'])) {

    $id = $_POST['update_id'];
    $name = $_POST['name'];
    $description = $_POST['description'];

    if (UpdateCategory($id, $name, $description) === TRUE) {
        echo '<script> alert("Data update"); </script>';
        #echo "Categoría actualizada correctamente";
    } else {
        echo '<script> alert("Data not update"); </script>';
        #echo "Error al actualizar categoría";
    }
}

$connection->close();
