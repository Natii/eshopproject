<?php
require("../logic/connection.php");
require("../class/category.php");
require("../views/indexCategory.php");
$connection = Connect();

if (isset($_POST['deleteData'])) {

    $id = $_POST['delete_id'];

    if (DeleteCategory($id) === TRUE) {
        echo '<script> alert("Data delete"); </script>';
        #echo "Categoría eliminada correctamente";
    } else {
        echo '<script> alert("Data not delete"); </script>';
        #echo "Error al eliminar categoría";
    }
}

$connection->close();
