<?php
session_start();
require("../logic/connection.php");
require("../class/user.php");
$user = $_SESSION['user'];

if (!$user) {
    header("Location: ../views/index.php");
}
?>

<a href="../logic/logout.php">Logout</a>

<?php
if ($user['email'] === 'admin@eshop.com') { 
    header("Location: ../views/indexAdmin.php");
    die();
} else { ?>
    <h1> Welcome <?php echo $user['name'];
                    echo " ";
                    echo $user['lastname'] ?> </h1>
<?php }
?>