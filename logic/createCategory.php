<?php
require("../logic/connection.php");
require("../class/category.php");
$connection = Connect();

if (isset($_POST['insertData'])) {

    $name = $_REQUEST['name'];
    $description = $_REQUEST['description'];

    $category = new Category($name, $description);

    if ($category->InsertCategory() === TRUE) {
        echo '<script> alert("Data create"); </script>';
        header("Location: indexCategory.php");
        #echo "Categoría registrada correctamente";
    } else {
        echo '<script> alert("Data not create"); </script>';
        #echo "Error al registrar categoría";
    }
}
$connection->close();
