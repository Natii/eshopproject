<?php
require("../logic/connection.php");
require("../class/user.php");

if ($_POST) {
    $email = $_REQUEST['email'];
    $password = $_REQUEST['password'];

    $user = AuthenticateUser($email, $password);

    if ($user) {
        session_start();
        $_SESSION['user'] = $user;
        header('Location: ../logic/dashboard.php');
        die();
    } else {
        header('Location: ../views/index.php?status=login');
        die();
    }
}
