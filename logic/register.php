<?php
require("../logic/connection.php");
require("../class/user.php");
$conn = Connect();
if ($_POST) {
    $name = $_REQUEST['name'];
    $lastname = $_REQUEST['lastname'];
    $phonenumber = $_REQUEST['phonenumber'];
    $email = $_REQUEST['email'];
    $address = $_REQUEST['address'];
    $password = $_REQUEST['password'];


    $user = new User($name, $lastname, $phonenumber, $email, $address, $password);
    if (mysqli_query($conn, $user->InsertUser())) {
        #header("Location: ../views/index.php");
        $message = "User successfully registered";
        echo "<script type='text/javascript'>alert('$message');</script>";
        #echo '<script language="javascript">alert("User successfully registered");window.location.href="../views/index.php"</script>';
    } else {
        $_SESSION['message'] = "Failed to register client";
        $_SESSION['message_type'] = "error";
    }
}
