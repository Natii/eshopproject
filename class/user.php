<?php
require_once("../logic/connection.php");
class User
{

    //Properties
    public $id;
    public $name;
    public $lastname;
    public $phoneNumber;
    public $email;
    public $address;
    public $password;

    function __construct($pName, $pLastname, $pPhoneNumber, $pEmail, $pAddress, $pPassword)
    {
        $this->name = $pName;
        $this->lastname = $pLastname;
        $this->phoneNumber = $pPhoneNumber;
        $this->email = $pEmail;
        $this->address = $pAddress;
        $this->password = $pPassword;
    }

    //Methods
    function InsertUser()
    {
        $connection = Connect();
        $sqlInsert = "INSERT INTO users (name, lastname, phone_number, email, address, password) 
            VALUES ('$this->name', '$this->lastname', '$this->phoneNumber', '$this->email', '$this->address', '$this->password');";
        $connection->query($sqlInsert);
        if ($connection->connect_errno) {
            $connection->close();
            return false;
        }
        $connection->close();
        return true;
    }

    function UpdateUser($pId)
    {
        $connection = Connect();
        $sqlUpdate = "UPDATE users SET name = '$this->name', lastname = '$this->lastname', phone_number = '$this->phoneNumber', email = '$this->email', address = '$this->address', password = '$this->password' WHERE id = '$pId';";
        $connection->query($sqlUpdate);
    }

    function SelectUsers()
    {
        $connection = Connect();
        $sqlSelect = "SELECT * FROM users;";
        $connection->query($sqlSelect);
    }

    function DeleteUser($pId)
    {
        $connection = Connect();
        $sqlDelete = "DELETE FROM users WHERE id = '$pId';";
        $connection->query($sqlDelete);
    }
}
function AuthenticateUser($email, $password)
{
    $conn = Connect();
    $sqlSelect = "SELECT * FROM users WHERE email = '$email' AND password = '$password';";
    $result = $conn->query($sqlSelect);

    if ($conn->connect_errno) {
        $conn->close();
        return false;
    }
    $conn->close();
    return $result->fetch_array();
}
