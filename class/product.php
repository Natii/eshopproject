<?php
require_once("../logic/connection.php");

class Product
{

    public $id;
    public $SKU;
    public $name;
    public $description;
    public $image;
    public $category;
    public $stock;
    public $price;

    function __construct($pSKU, $pName, $pDescription, $pImage, $pCategory, $pStock, $pPrice)
    {
        $this->SKU = $pSKU;
        $this->name = $pName;
        $this->description = $pDescription;
        $this->image = $pImage;
        $this->category = $pCategory;
        $this->stock = $pStock;
        $this->price = $pPrice;
    }

    //Methods
    function InsertProduct()
    {
        $connection = Connect();
        $sqlInsert = "INSERT INTO products (sku, name, description, image, category, stock, price) 
                VALUES ('$this->SKU', '$this->name', '$this->description', '$this->image', '$this->category', '$this->stock', '$this->price');";
        $connection->query($sqlInsert);
        if ($connection->connect_errno) {
            $connection->close();
            return false;
        }
        $connection->close();
        return true;
    }

    function UpdateProduct($pId)
    {
        $connection = Connect();
        $sqlUpdate = "UPDATE products SET name = '$this->name', description = '$this->description', image = '$this->image', category = '$this->category', stock = '$this->stock', price = '$this->price' WHERE id = '$pId';";
        $connection->query($sqlUpdate);
    }

    function SelectProducts()
    {
        $connection = Connect();
        $sqlSelect = "SELECT * FROM products;";
        $connection->query($sqlSelect);
    }

    function DeleteProduct()
    {
        $connection = Connect();
        $sqlDelete = "DELETE FROM products WHERE id = '$this->id';";
        $connection->query($sqlDelete);
    }
}
