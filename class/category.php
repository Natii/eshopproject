<?php
require_once("../logic/connection.php");
$connection = Connect();

class Category
{
    //Category Properties
    public $categoryName;
    public $majorCategory;

    function __construct($pCategoryName, $pMajorCategory)
    {
        $this->categoryName = $pCategoryName;
        $this->majorCategory = $pMajorCategory;
    }

    //Category Methods
    function InsertCategory()
    {
        $connection = Connect();
        $sqlInsert = "INSERT INTO categories (name, major_category) VALUES ('$this->categoryName', '$this->majorCategory')";
        $connection->query($sqlInsert);

        if ($connection->connect_errno) {
            $connection->close();
            return false;
        }
        $connection->close();
        return true;
    }
}

function UpdateCategory($pId, $pName, $pMajorCategory)
{
    $connection = Connect();
    $sqlUpdate = "UPDATE categories SET name = '$pName', major_category = '$pMajorCategory' WHERE id = '$pId'";
    $connection->query($sqlUpdate);

    if ($connection->connect_errno) {
        $connection->close();
        return false;
    }
    $connection->close();
    return true;
}

function DeleteCategory($pId)
{
    $connection = Connect();
    $sqlDelete = "DELETE FROM categories WHERE id = '$pId'";
    $connection->query($sqlDelete);

    if ($connection->connect_errno) {
        $connection->close();
        return false;
    }
    $connection->close();
    return true;
}
